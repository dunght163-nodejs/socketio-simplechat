# SOCKET.IO SIMPLE CHAT

Welcome to the Simple chat application using Socket.io!

---

## Requirements

1. Node >= 16

## Steps

Install libraries

```
npm install
```

Start application

```
npm run start
```

Open application on [localhost:3000](localhost:3000) and enjoy!

## Homework

Here are some ideas to improve the application:

1. &reg; Broadcast a message to connected users when someone connects or disconnects.
2. &reg; Add support for nicknames.
3. &reg; Don’t send the same message to the user that sent it. Instead, append the message directly as soon as they press
   enter.
4. &reg; Add “{user} is typing” functionality.
5. &reg; Show who’s online.
6. Add private messaging.
7. Share your improvements!

---

## References

- [socket.io get-started chat](https://socket.io/get-started/chat)