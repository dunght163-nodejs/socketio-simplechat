/* import libraries */
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require('socket.io');
const {response} = require("express");
const io = new Server(server);

/* define css resource */
app.use('/css', [
    express.static(__dirname + '/node_modules/bootstrap/dist/css/'),
    express.static(__dirname + '/css/'),
]);

/* define js resource */
app.use('/js', [
    express.static(__dirname + '/node_modules/bootstrap/dist/js/'),
    express.static(__dirname + '/js/'),
    express.static(__dirname + '/node_modules/socket.io/client-dist/'),
]);

/* homepage support html file */
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

/* init io listening */
let connectedUsers = {};
io.on('connection', (socket) => {
    console.log(`An user connected at socket ${socket.id}, handshaking...`);

    /* send event to all users */
    socket.emit("handshake", "Who you are?");
    socket.on('handshake', (msg) => {
        console.log(`Received from "handshake": `, msg);
        if (msg.userId && msg.username) {
            connectedUsers[socket.id] = {socket: socket.id, ...msg};
            console.log(`connectedUsers: `, Object.keys(connectedUsers), connectedUsers);
            io.emit("events", {
                total: Object.keys(connectedUsers).length,
                eventType: 'connected',
                data: {
                    userId: msg.userId,
                    username: msg.username,
                }
            });
        }
    });

    /* broadcast except for a certain emitting socket */
    socket.broadcast.emit(`Hi, this is a broadcast message`);

    /* handle chat message */
    socket.on('chat message', (msg) => {
        console.log(`Receive new chat message: ${msg}, now forward to other users...`);
        io.emit('chat message', msg);
    });

    /* handle events */
    const FORWARDABLE_EVENTS = [
        'typing',
    ];
    socket.on('events', (msg) => {
        console.log(`Receive new event: `, msg);
        if (!msg.eventType || !msg.data) {
            console.log(`-> invalid event format`);
            return;
        }
        if (!FORWARDABLE_EVENTS.includes(msg.eventType)) {
            console.log(`-> not supported event ${msg.eventType} => skipped forwarding`);
            return;
        }

        console.log(`-> forwarding to rest users`);
        io.emit("events", msg);
    });

    /* handle disconnect event */
    socket.on('disconnect', (reason) => {
        console.log(`Connection ${socket.id} disconnected due to ${reason}`);
        let disconnectedUser = connectedUsers[socket.id];
        delete connectedUsers[socket.id];
        io.emit("events", {
            total: Object.keys(connectedUsers).length,
            eventType: 'disconnected',
            data: {
                userId: disconnectedUser.userId,
                username: disconnectedUser.username,
            }
        });
    });
});

/* start simplest server */
server.listen(3000, () => {
    console.log('Listening on *.3000...');
});

