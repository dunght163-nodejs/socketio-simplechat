console.log('app.js');

function zeroPad(input, length) {
    return (Array(length + 1).join('0') + input).slice(-length);
}

function randIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randUserId() {
    return 'u' + zeroPad(randIntInRange(1e5, 1e10), 10);
}